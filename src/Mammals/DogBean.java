package Mammals;

// Make class Mammals.DogBean
public class DogBean extends MammalBean {
    private String breed;
    private String name;
    public DogBean(int legCount, String color, double height) {
        super(legCount, color, height);

    }

    // getters and setters for Mammals.DogBean
    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Constructor

    public DogBean(int legCount, String color, double height, String breed, String name) {
        super(legCount, color, height);
        this.breed = breed;
        this.name = name;
    }
    // toString method
    @Override
    public String toString() {
        return String.format ("%n%n LegCount= " + getLegCount() + "%n Color= " + getColor() + "%n Height= " + getHeight() + "%n Breed= "+ getBreed() + "%n Name= " + getName());
    }
}

