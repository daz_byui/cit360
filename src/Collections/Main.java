package Collections;

import java.util.*;
import java.io.*;

public class Main {
    //Shorten print
    PrintStream out = System.out;

    public static void main(String[] args) {
        // Path to the CVS file
        File file = new File("C:/Users/dbutt/Google Drive/BYUI/CIT 360/repo/rawData.csv");
        BufferedReader buffRead = null;
        String line;
        // Create Tree Map
        TreeMap<String, Account> tree = new TreeMap<String, Account>();
        try {
            buffRead = new BufferedReader(new FileReader(file));
            while ((line = buffRead.readLine()) != null) {

                String[] split = line.split(",");
                if (!tree.containsKey(split[0])) {
                    Account account = new Account(split[0], split[1], split[2], split[3]);
                    tree.put(split[0], account);
                }
            }
            for (Map.Entry<String, Account> entry : tree.entrySet()) {
                System.out.println(entry.getValue().toString());
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        catch (IOException e) {
            e.printStackTrace();
        }
            finally{
                if (buffRead != null){
                    try{
                        buffRead.close();
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }