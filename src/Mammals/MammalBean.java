package Mammals;

public class MammalBean {
    private int legCount;
    private String color;
    private double height;

    // Constructor


    public MammalBean(int legCount, String color, double height) {
        this.legCount = legCount;
        this.color = color;
        this.height = height;
    }
        // getters and setters


        public int getLegCount() {
            return legCount;
        }public void setLegCount(int legCount) {
            this.legCount = legCount;
        }public String getColor() {
            return color;
        }public void setColor(String color) {
            this.color = color;
        }public double getHeight() {
            return height;
        }public void setHeight(double height) {
            this.height = height;
        }


}


