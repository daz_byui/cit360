package Mammals;

public class Kennel{

    public static void main(String[] args){
        Kennel kennel = new Kennel();
        DogBean[] allDogs = kennel.buildDogs();
        kennel.displayDogs(allDogs);

    }

    private DogBean[] buildDogs(){
        DogBean dog1 = new DogBean(4,"White", 21.5,"Collie","Lassie");
        DogBean dog2 = new DogBean(4,"Red", 300.0,"Big Dog","Cliford");
        DogBean dog3 = new DogBean(4,"Black", 22.0,"Siberian Husky","Balto");
        DogBean dog4 = new DogBean(4,"Brown", 63.6,"German Shepherd","Rin Tin Tin");
        DogBean dog5 = new DogBean(4,"White", 14.4,"Jack Russell Terrier","Wishbone");

        DogBean[] dogArray = {dog1,dog2,dog3,dog4,dog5};
        // returns
        return dogArray;
    }

    private void displayDogs(DogBean[] dogArray) {

        for(int i=0; i<dogArray.length; i++){
            System.out.print(dogArray[i]);
        }
    }

}
