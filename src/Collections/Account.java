package Collections;

import java.io.PrintStream;





public class Account {
    // Declare the accountNumber, accountType, givenName, and familyName attributes.
    private String accountNumber;
    private String accountType;
    private String givenName;
    private String familyName;



    Account(String accountNumber, String accountType, String givenName, String familyName){
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @Override
    public String toString() {
        return String.format("%n%nAccount Number: "+ accountNumber + "%nAccount Type: " + accountType + "%nFirst Name: " + givenName + "%nLast Name: " + familyName);
    }
}


