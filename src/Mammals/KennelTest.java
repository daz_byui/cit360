package Mammals;

import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class KennelTest {
    // Part 1. Test Mammal and Dog

    @Test
    public void dog1() throws Exception {
        DogBean dog1 = new DogBean(4,"White", 21.5,"Collie","Lassie");
        // Asserts for dog1
        assertEquals(4, dog1.getLegCount());
        assertEquals("White", dog1.getColor());
        assertEquals(21.5, dog1.getHeight(),0.5);
        assertEquals("Collie",dog1.getBreed());
        assertEquals("Lassie",dog1.getName());
    }

    @Test
    public void dog2() throws Exception {
        DogBean dog2 = new DogBean(4,"Red", 300.0,"Big Dog","Cliford");
        // Asserts for dog2
        assertEquals(4, dog2.getLegCount());
        assertEquals("Red", dog2.getColor());
        assertEquals(300.5, dog2.getHeight(),0.5);
        assertEquals("Big Dog",dog2.getBreed());
        assertEquals("Cliford",dog2.getName());
    }

    @Test
    public void dog3() throws Exception {
        DogBean dog3 = new DogBean(4,"Black", 22.0,"Siberian Husky","Balto");
        // Asserts for dog3
        assertEquals(4, dog3.getLegCount());
        assertEquals("Black", dog3.getColor());
        assertEquals(22.0, dog3.getHeight(),0.5);
        assertEquals("Siberian Husky",dog3.getBreed());
        assertEquals("Balto",dog3.getName());
    }

    @Test
    public void mam1() throws Exception {
        MammalBean mam1 = new MammalBean(1,"White",60.1);
        // Asserts for mam1
        assertEquals(1,mam1.getLegCount());
        assertEquals("White",mam1.getColor());
        assertEquals(60.1,mam1.getHeight(),0.5);

    }

    @Test
    public void mam2() throws Exception {
        MammalBean mam2 = new MammalBean(2,"Yellow",50.1);
        // Asserts for mam2
        assertEquals(2,mam2.getLegCount());
        assertEquals("Yellow",mam2.getColor());
        assertEquals(50.1,mam2.getHeight(),0.5);

    }

    @Test
    public void mam3() throws Exception {
        MammalBean mam3 = new MammalBean(3,"Black",80.1);
        // Asserts for mam1
        assertEquals(3,mam3.getLegCount());
        assertEquals("Black",mam3.getColor());
        assertEquals(80.1,mam3.getHeight(),0.5);
    }

    @Test
    public void MammalSet() {
        // Create Mammal Objects
        MammalBean mam1 = new MammalBean(1,"White",60.1);
        MammalBean mam2 = new MammalBean(2,"Yellow",50.1);
        MammalBean mam3 = new MammalBean(3,"Black",80.1);
        // Create a hash set to store mammals in
        Set<MammalBean> hashSet = new HashSet<>();
        // Add mammal objects to hashSet
        hashSet.add(mam1);
        hashSet.add(mam2);
        hashSet.add(mam3);
        assertTrue(hashSet.contains(mam1));
        assertTrue(hashSet.contains(mam2));
        assertTrue(hashSet.contains(mam3));
        // remove hashSet
        hashSet.remove(mam2);
        hashSet.remove(mam3);
        assertTrue(hashSet.contains(mam1));
        assertFalse(hashSet.contains(mam2));
        assertFalse(hashSet.contains(mam3));
    }

    @Test
    public void DogMap() {
        // Create Dog Objects
        DogBean dog1 = new DogBean(4,"White", 21.5,"Collie","Lassie");
        DogBean dog2 = new DogBean(4,"Red", 300.0,"Big Dog","Cliford");
        DogBean dog3 = new DogBean(4,"Black", 22.0,"Siberian Husky","Balto");
        DogBean dog4 = new DogBean(4,"Brown", 63.6,"German Shepherd","Rin Tin Tin");
        DogBean dog5 = new DogBean(4,"White", 14.4,"Jack Russell Terrier","Wishbone");
        // Create hashMap
        Map<String, DogBean> hashMap = new HashMap<>();
        hashMap.put("Lassie",dog1);
        hashMap.put("Cliford",dog2);
        hashMap.put("Balto",dog3);
        hashMap.put("Rin Tin Tin",dog4);
        // assert for add Map
        assertTrue(hashMap.containsKey("Lassie"));
        assertTrue(hashMap.containsKey("Cliford"));
        assertTrue(hashMap.containsKey("Balto"));
        assertTrue(hashMap.containsKey("Rin Tin Tin"));
        // remove from hashMap
        hashMap.remove("Lassie",dog1);
        hashMap.remove("Cliford",dog2);
        // assert for removed dogs
        assertFalse(hashMap.containsKey("Lassie"));
        assertFalse(hashMap.containsKey("Cliford"));
        assertTrue(hashMap.containsKey("Balto"));
        assertTrue(hashMap.containsKey("Rin Tin Tin"));




    }

}